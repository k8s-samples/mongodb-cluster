# Mongodb Cluster
=================

# Mongodb Cluster
=================

Important:

- The yaml file or other configuration include the namespace.
- The order of execution must be respected
- The "conf" folder contains configuration commands that must be applied in the mongodb console.

Official information [mongodb sharded howto](https://docs.mongodb.com/manual/tutorial/deploy-shard-cluster/).

Execution order:

1. Service

2. Configsrv

3. Replset

4. Mongos

## Start MongoDB

### 1 - Services

```
$ kubectl -n ops-graylog apply -f svc-graylogdb-configsrv.yaml
$ kubectl -n ops-graylog apply -f svc-graylogdb-mongos.yaml
$ kubectl -n ops-graylog apply -f svc-graylogdb-replset.yaml
```

### 2 - configsrv

```
$ kubectl -n ops-graylog apply -f sts-graylog-configsrv.yaml
```

Validate that the 3 pod replicas are running:

```
$ kubectl -n ops-graylog get pod
NAME                       READY     STATUS    RESTARTS   AGE
mongodb-configsrv-0        1/1       Running   0          17h
mongodb-configsrv-1        1/1       Running   0          17h
mongodb-configsrv-2        1/1       Running   0          17h
```

Enter the pod console:

```
kubectl -n ops-graylog exec -ti mongodb-configsrv-0 bash
```

Enter the mongo console:

```
mongo --port 27019 --host $(hostname -f)
```

Finally execute the commands that are in "prod/conf/rs_initiate_configsrv.mongo"


### 3 - Replset

```
$ kubectl -n ops-graylog apply -f sts-graylog-replset.yaml
```

Validate that the 3 pod replicas are running:

```
$ kubectl -n ops-graylog get pod
NAME                       READY     STATUS    RESTARTS   AGE
mongodb-replset-0          1/1       Running   0          17h
mongodb-replset-1          1/1       Running   0          17h
mongodb-replset-2          1/1       Running   0          17h

```

Enter the pod console:

```
kubectl -n ops-graylog exec -ti mongodb-replset-0 bash
```

Enter the mongo console:

```
mongo --port 27018 --host $(hostname -f)
```

Finally execute the commands that are in "prod/conf/rs_initiate_replset.mongo"


### 4 - Mongos

```
$ kubectl -n ops-graylog apply -f sts-graylog-mongos.yaml
```
Validate that the pod is running:

```
$ kubectl -n ops-graylog get pod
NAME                       READY     STATUS    RESTARTS   AGE
mongodb-mongos-0           1/1       Running   2          17h

```

Enter the pod console:

```
kubectl -n ops-graylog exec -ti mongodb-mongos-0 bash
```

Enter the mongo console:

```
mongo --port 27017 --host $(hostname -f)
```

Finally execute the commands that are in "prod/conf/rs_initiate_mongos.mongo"
